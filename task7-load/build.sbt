name := "task7-load"

version := "0.1"

scalaVersion := "2.12.10"


libraryDependencies ++= Seq("org.scala-lang.modules" %% "scala-xml" % "1.2.0",
  "mysql" % "mysql-connector-java" % "8.0.19",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "com.amazonaws" % "aws-java-sdk" % "1.11.46"
)

mainClass in (Compile, packageBin) := Some("Main")

mainClass in (Compile, run) := Some("Main")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
