import controller.LoadController

object Main {

  def main(args: Array[String]): Unit = {

    val loadController = new LoadController(args(0), args(1))
    loadController.process()

    }

}
