package controller

import java.text.SimpleDateFormat
import java.util.Date


class DataParserCSV(delimiter: String, openPriceID: Int, lowPriceID: Int, highPriceID: Int,
                    closePriceID: Int, volumeID: Int,
                    dateColumns: List[Int], dateFormats: List[String]) {

 def parse(line: String): (Date, Float, Float, Float, Float, Int) = {

   val values = line.split(delimiter)

   val openPrice = values(openPriceID).replace(",", ".").toFloat
   val lowPrice = values(lowPriceID).replace(",", ".").toFloat
   val highPrice = values(highPriceID).replace(",", ".").toFloat
   val closePrice = values(closePriceID).replace(",", ".").toFloat
   val volume = values(volumeID).replace(",", "").toInt


   val dateBuilder = new StringBuilder()

   for (i <- dateColumns) {
     dateBuilder ++= (values(i) + " ")
   }

   dateBuilder.deleteCharAt(dateBuilder.length - 1)
   val dateFormat = dateFormats.mkString(" ")

   val simpleDateFormat = new SimpleDateFormat(dateFormat)
   val parsedDate = simpleDateFormat.parse(dateBuilder.toString())


   (parsedDate, openPrice, highPrice, lowPrice, closePrice, volume)
 }

}
