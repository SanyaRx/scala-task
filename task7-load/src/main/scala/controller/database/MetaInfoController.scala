package controller.database

import java.sql._
import java.text.SimpleDateFormat


class MetaInfoController(connector: JDBCConnector) {

  final private val INSERT_FIRST_STAGE = "INSERT INTO etl_info(source, destination, start_date, process_name)" +
    "VALUES(?, ?, ?, ?)"

  final private val UPDATE_FIRST_STAGE = "UPDATE etl_info " +
    "SET finish_date = ?, " +
    "input_rows = ?, " +
    "processed_rows = ?, " +
    "source_checksum = ? " +
    "WHERE id = ?"


  final private val GET_FILE_HASHSUM = "SELECT source_checksum FROM etl_info " +
    "WHERE source = ? " +
    "and destination = ? " +
    "and finish_date = (SELECT max(finish_date) from etl_info where source = ? and destination = ?)"

  final private val CLEAR_ETL_INFO = "DELETE FROM etl_info"


  def insertInfo(source: String, destination: String, startDate: Long, processName: String): Int = {
    var connection:Connection = null

    try {
      connection = connector.getConnection

      val ps = connection.prepareStatement(INSERT_FIRST_STAGE, Statement.RETURN_GENERATED_KEYS)
      val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

      ps.setString(1, source)
      ps.setString(2, destination)

      ps.setString(3, format.format(new Date(startDate)))

      ps.setString(4, processName)

      ps.executeUpdate

      val rs = ps.getGeneratedKeys
      var generatedKey = 0

      if (rs.next) generatedKey = rs.getInt(1)

      ps.close()

      generatedKey
    } catch {

      case e: Exception =>
        e.printStackTrace()
        -1
      case e: SQLException =>
        System.err.println(e.getMessage)
        e.printStackTrace()
        -1

    } finally try connection.close()
    catch {

      case e: SQLException =>
        e.printStackTrace()

    }
  }

  def updateInfo(finishedDate: Long, inputRows: Long, processedRows: Long, checksum: String, id: Int): Unit = {
    var connection:Connection = null

    try {
      connection = connector.getConnection

      val ps = connection.prepareStatement(UPDATE_FIRST_STAGE)

      val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

      ps.setString(1, format.format(new Date(finishedDate)))
      ps.setLong(2, inputRows)
      ps.setLong(3, processedRows)

      ps.setString(4, checksum)
      ps.setInt(5, id)

      ps.executeUpdate

      ps.close()

    } catch {

      case e: Exception =>
        e.printStackTrace()
      case e: SQLException =>
        System.err.println(e.getMessage)
        e.printStackTrace()

    } finally try connection.close()
    catch {

      case e: SQLException =>
        e.printStackTrace()

    }
  }


  def getHashSum(source: String, destination: String) : String = {
    var connection:Connection = null

    try {
      connection = connector.getConnection

      val ps = connection.prepareStatement(GET_FILE_HASHSUM)

      ps.setString(1, source)
      ps.setString(3, source)

      ps.setString(2, destination)
      ps.setString(4, destination)


      val rs = ps.executeQuery()

      var hashSum: String = null

      if(rs.next()) hashSum = rs.getString(1)

      ps.close()
      hashSum


    } catch {

      case e: Exception =>
        e.printStackTrace()
        null
      case e: SQLException =>
        System.err.println(e.getMessage)
        e.printStackTrace()
        null

    } finally try connection.close()
    catch {

      case e: SQLException =>
        e.printStackTrace()

    }
  }

  def clearTable(query: String): Unit = {
    var connection: Connection = null
    try {
      connection = connector.getConnection
      val ps = connection.prepareStatement(query)
      ps.executeUpdate
      ps.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    } finally {
      connection.close()
    }
  }
}