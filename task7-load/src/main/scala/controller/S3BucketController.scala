package controller


import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.transfer.TransferManager


class S3BucketController(awsS3Client: AmazonS3) {


  def getListOfObjectSummaries(prefix: String, bucketName: String) = {

    //val tm = new TransferManager(awsS3Client)

    println(bucketName)
    println(awsS3Client.listBuckets().toArray.map(_.toString).mkString("\n"))

    val listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix)//.withDelimiter("/")
    val objectListing = awsS3Client.listObjects(listObjectsRequest)

    objectListing.getObjectSummaries
  }

  def readS3Object(bucketName: String, key: String) = awsS3Client.getObject(bucketName, key).getObjectContent

  def doesFileExists(bucketName: String , filePath: String): Boolean = awsS3Client.doesObjectExist(bucketName, filePath)



}
