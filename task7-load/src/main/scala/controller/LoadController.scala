package controller

import java.io.{File, FileInputStream, InputStream}
import java.security.{DigestInputStream, MessageDigest}
import java.text.SimpleDateFormat
import java.util.Date
import com.typesafe.scalalogging.Logger
import controller.config.{DBConfigReader, DataDescriptionReader, LoadConfigReader}
import controller.database.MetaInfoController

class LoadController(configPath: String, auditConfigPath: String) {

  val logger = Logger("controller")

  private val header = "date,open,high,low,close,vol"
  private val dataDescription = "<data_description>\n" +
    "<currency>xau_usd</currency>\n" +
    "   <delimiter>,</delimiter>\n" +
    "   <open_price>1</open_price>\n" +
    "   <high_price>2</high_price>\n" +
    "   <low_price>3</low_price>\n" +
    "   <close_price>4</close_price>\n" +
    "   <volume>5</volume>\n " +
    "   <date>\n" +
    "        <columns>\n" +
    "            <value>0</value>\n" +
    "        </columns>\n" +
    "        <formats>\n" +
    "            <value>yyyy.MM.dd HH:mm:ss</value>\n" +
    "        </formats>\n" +
    "    </date>\n" +
    "</data_description>"

  def computeHash(file: File): String = {
    val buffer = new Array[Byte](8192)
    val md5 = MessageDigest.getInstance("MD5")

    val dis = new DigestInputStream(new FileInputStream(file), md5)
    try { while (dis.read(buffer) != -1) { } } finally { dis.close() }

    md5.digest.map("%02x".format(_)).mkString
  }


  def groupRows(line: (Long, Array[(Date, Float, Float, Float, Float, Int)])) = {
    val sortedArray = line._2.sortWith(_._1.getTime < _._1.getTime)

    val date = sortedArray(0)._1
    val openPrice = sortedArray(0)._2
    val highPrice = sortedArray.maxBy(_._3)._3
    val lowPrice = sortedArray.minBy(_._4)._4
    val closePrice = sortedArray.last._5
    val volume = sortedArray.map(_._6).sum

    (date, openPrice, highPrice, lowPrice, closePrice, volume)
  }

  def toCSVLine(line: (Date, Float, Float, Float, Float, Int)): String = {
    val dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
    val date = dateFormat.format(line._1)
    val openPrice = line._2
    val highPrice = line._3
    val lowPrice = line._4
    val closePrice = line._5
    val volume = line._6

    date + "," + openPrice + "," + highPrice + "," + lowPrice + "," + closePrice + "," + volume
  }

  def readCSV(fileLines: Iterator[String], header: Boolean, parser: DataParserCSV) : Array[String] = {

    val linesToDrop = if (header) 1 else 0

    fileLines.drop(linesToDrop)
      .map(parser.parse)
      .toArray
      .groupBy(line => line._1.getTime - line._1.getSeconds * 1000 )
      .map(groupRows)
      .toArray
      .sortWith(_._1.getTime < _._1.getTime)
      .map(toCSVLine)


  }

  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }


  def inputToFile(is: InputStream, f: File) {
    val in = scala.io.Source.fromInputStream(is)
    val out = new java.io.PrintWriter(f)
    try { in.getLines().foreach(out.println(_)) }
    finally { out.close }
  }

  def process(): Unit = {

    val loadConfigReader = new LoadConfigReader(configPath)
    loadConfigReader.parse()

    val sqlConfigReader = new DBConfigReader(auditConfigPath)
    sqlConfigReader.parse()


    val sourceBucketController = new S3BucketController(loadConfigReader.sourceUser)
    val destinationBucketController = new S3BucketController(loadConfigReader.destinationUser)

    val objectsSummaries = sourceBucketController.getListOfObjectSummaries(loadConfigReader.sourcePath, loadConfigReader.sourceBucketName)

    val dataConfigFilePath = loadConfigReader.sourcePath + "/" + loadConfigReader.dataConfigFile
    val dataConfigFile = sourceBucketController.readS3Object(loadConfigReader.sourceBucketName,
      dataConfigFilePath )


    val dataConfigReader = new DataDescriptionReader(dataConfigFile)
    dataConfigReader.parse()

    val objectKeys = objectsSummaries.stream()
      .map[String](_.getKey)
      .filter(_.endsWith(".csv"))
      .toArray
      .map(_.toString)

    val dbController = new MetaInfoController(sqlConfigReader.connector)

    loadConfigReader.destinationUser.putObject(loadConfigReader.destinationBucketName,
      loadConfigReader.destinationPath + "/" +  dataConfigReader.currency + "/" + "data_description.xml", dataDescription)

    for (key <- objectKeys) {
      val source = "s3a//:" + loadConfigReader.sourceBucketName + "/" + key

      val destinationFilePath = loadConfigReader.destinationPath + "/" + dataConfigReader.currency + "/" + key.split("/").last

      val destination = "s3a//:" + loadConfigReader.destinationBucketName + "/" + destinationFilePath

      val destinationFileExists = destinationBucketController.doesFileExists(loadConfigReader.destinationBucketName,
        destinationFilePath)

      logger.info(s"Working with file: $source")

      val sourceInputStream = sourceBucketController.readS3Object(loadConfigReader.sourceBucketName, key)

      val tempDataFile = new File(key.split("/").last)

      logger.info("Downloading file to local directory")

      inputToFile(sourceInputStream, tempDataFile)

      val sourceHash = dbController.getHashSum(source, destination)
      val currentSourceHash = computeHash(tempDataFile)

      if (!destinationFileExists || sourceHash == null || !sourceHash.equals(currentSourceHash)) {

        val startDate = System.currentTimeMillis()
        val currentID = dbController.insertInfo(source, destination, startDate, "etl0")


        val bufferedSource = io.Source.fromFile(tempDataFile)
        val fileIterators = bufferedSource.getLines.duplicate
        val inputRows = fileIterators._1.length

        logger.info("Reading file")
        val rows = readCSV(fileIterators._2, header = true, dataConfigReader.dataParser)

        logger.info(s"Uploading file to $destinationFilePath")
        loadConfigReader.destinationUser.putObject(loadConfigReader.destinationBucketName, destinationFilePath, (header +: rows).mkString("\n"))

        val finishDate = System.currentTimeMillis()
        val processedRows = rows.length

        dbController.updateInfo(finishDate, inputRows, processedRows, currentSourceHash, currentID)

        bufferedSource.close()
      } else {
        logger.info("File is already up to date")
      }
      logger.info("Deleted: " + tempDataFile.delete())
    }

  }

}
