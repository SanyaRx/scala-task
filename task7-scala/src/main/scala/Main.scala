import controller.ScalaController

object Main {



  def main(args: Array[String]): Unit = {

    val scalaController = new ScalaController(args(0), args(1))

    scalaController.executeTask()

  }

}
