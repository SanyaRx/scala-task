package model

import java.time.LocalDate


/**
  * Normalized CandleKaggleLine class
  **/
class CandlePrice(var year: Short, var month: Short, var day: Short,
                  var timestamp: Int, var openPrice: Float, var closePrice: Float) {

  def this(candle: CandlePrice) {
    this(candle.year, candle.month, candle.day, candle.timestamp, candle.openPrice, candle.closePrice)
  }

  def getDayOfWeek: Int = LocalDate.of(year, month, day).getDayOfWeek.getValue

  override def toString: String = {
    val hours = timestamp / 3600
    val minutes = (timestamp - 3600 * hours) / 60
    val seconds = timestamp - 3600 * hours - 60 * minutes
    year + "." + month + "." + day + ";" + hours + ":" + minutes + ":" + seconds + ";" + openPrice + ";" + closePrice
  }
}
