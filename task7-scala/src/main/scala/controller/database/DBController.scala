package controller.database

import java.sql.{Connection, Date, SQLException, Statement}


class DBController(connector: JDBCConnector) {

  private val months = Array[String]("january", "february", "march",
  "april", "may", "june", "july", "august", "september", "october", "november", "december")

  private val INSERT_YEAR_MONTH = "INSERT INTO year_month_report(year, january, february, march, " +
    "april, may, june, july, august, september, october, november, december, total, currency, source) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

  private val INSERT_WEEKDAY_MONTH = "INSERT INTO weekday_month_report(weekday, january, february, march, " +
    "april, may, june, july, august, september, october, november, december, total, currency, source) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"


  private var DELETE_OUTDATED_ROWS = "DELETE FROM year_month_report " +
    "WHERE source = ?"

  private val CLEAR_YEAR_MONTH = "DELETE FROM year_month_report"
  private val CLEAR_WEEKDAY_MONTH = "DELETE FROM weekday_month_report"




  def insertYearMonth(data: Array[String]): Unit = {
    insertIntoTable(data, "year_month_report")
  }

  def insertWeekdayMonth(data: Array[String]): Unit = {
    insertIntoTable(data, "weekday_month_report")
  }

  private def insertIntoTable(data: Array[String], tableName: String): Unit = {
    var connection:Connection = null

    val queryBuilder = new StringBuilder

    queryBuilder.append(s"INSERT INTO $tableName(year, ")
    queryBuilder.append(months.slice(0, data.length - 4).mkString(", "))
    queryBuilder.append(", total, currency, source) VALUES(")
    queryBuilder.append("?, " * (data.length - 1))
    queryBuilder.append("?)")

    println(queryBuilder)

    try {
      connection = connector.getConnection

      val ps = connection.prepareStatement(queryBuilder.toString())

      for (i <- data.indices) {
        ps.setString(i + 1, data(i))
      }

      ps.executeUpdate
      ps.close()
    } catch {

      case e: Exception =>
        e.printStackTrace()
      case e: SQLException =>
        System.err.println(e.getMessage)
        e.printStackTrace()

    } finally try connection.close()
    catch {

      case e: SQLException =>
        e.printStackTrace()

    }
  }

  def deleteOutdatedRows(source: String) = {
    var connection:Connection = null

    try {
      connection = connector.getConnection

      val ps = connection.prepareStatement(DELETE_OUTDATED_ROWS)

      ps.setString(1, source)

      ps.executeUpdate

      ps.close()

    } catch {

      case e: Exception =>
        e.printStackTrace()
      case e: SQLException =>
        System.err.println(e.getMessage)
        e.printStackTrace()

    } finally try connection.close()
    catch {

      case e: SQLException =>
        e.printStackTrace()

    }
  }


  def clearYearMonthTable(): Unit = {
    clearTable(CLEAR_YEAR_MONTH)
  }

  def clearWeekdayMonthTable(): Unit = {
    clearTable(CLEAR_WEEKDAY_MONTH)
  }

  private def clearTable(query: String): Unit = {
    var connection: Connection = null
    try {
      connection = connector.getConnection
      val ps = connection.prepareStatement(query)
      ps.executeUpdate
      ps.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    } finally {
      connection.close()
    }
  }
}