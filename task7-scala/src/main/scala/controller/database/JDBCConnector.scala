package controller.database

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

/**
  * Database connector
  * */
class JDBCConnector(url:String, driver:String, user:String, pass: String) {


  /** Connection to database */
  private var conn: Connection = _


  /**
    * Returns connection to database
    **/
  @throws[Exception]
  def getConnection: Connection = {

    try {
      Class.forName(driver)
      conn = DriverManager.getConnection(url, user, pass)
    } catch {
      case e: ClassNotFoundException =>
        throw new Exception("Can't load database driver.", e)
      case e: SQLException =>
        throw new Exception("Can't connect to database.", e)
      case e =>
        e.printStackTrace()
    }

    if (conn == null) throw new Exception("Driver type is not correct in URL " + url + ".")

    conn
  }


  /**
    * Closes connection to database
    **/
  @throws[Exception]
  def close(): Unit = {
    if (conn != null) try conn.close
    catch {
      case e: SQLException =>
        throw new Exception("Can't close connection", e)
    }
  }
}
