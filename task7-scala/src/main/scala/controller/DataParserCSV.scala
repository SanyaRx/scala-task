package controller

import java.time.{LocalDateTime}
import java.time.format.DateTimeFormatter

import model.CandlePrice

class DataParserCSV(delimiter: String, openPriceID: Int, closePriceID: Int,
                    dateColumns: List[Int], dateFormats: List[String]) {

 def parse(line: String): CandlePrice = {

   val values = line.split(delimiter)

   val openPrice = values(openPriceID).replace(",", ".").toFloat
   val closePrice = values(closePriceID).replace(",", ".").toFloat

   val dateBuilder = new StringBuilder()

   for (i <- dateColumns) {
     dateBuilder ++= (values(i) + " ")
   }

   dateBuilder.deleteCharAt(dateBuilder.length - 1)

   val dateFormat = dateFormats.mkString(" ")

   val date = LocalDateTime.parse(dateBuilder.toString(), DateTimeFormatter.ofPattern(dateFormat))

   val year = date.getYear.toShort
   val month = date.getMonthValue.toShort
   val day = date.getDayOfMonth.toShort

   val timestamp: Int = 3600 * date.getHour + 60 * date.getMinute
   + date.getSecond

   new CandlePrice(year, month, day, timestamp, openPrice, closePrice)

 }

}
