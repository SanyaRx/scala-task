package controller.comparator.group

import model.CandlePrice

/**
  * Compares two CandlePrice objects by year
  **/
class YearComparator extends Ordering[CandlePrice] {
  override def compare(o1: CandlePrice, o2: CandlePrice): Int = o1.year.compare(o2.year)
}

