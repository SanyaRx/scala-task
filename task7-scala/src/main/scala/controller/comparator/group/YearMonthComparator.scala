package controller.comparator.group

import model.CandlePrice

/**
  * Compares two CandlePrice objects by year and month
  **/
class YearMonthComparator extends Ordering[CandlePrice] {
  override def compare(o1: CandlePrice, o2: CandlePrice): Int = {
    if (o1.year != o2.year) return o1.year.compare(o2.year)

    o1.month.compare(o2.month)
  }
}