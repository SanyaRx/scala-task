package controller.comparator

import model.CandlePrice

class DateComparator extends Ordering[CandlePrice] {
  override def compare(o1: CandlePrice, o2: CandlePrice): Int = {
    if (o1.year != o2.year) return o1.year.compare(o2.year)
    else if (o1.month != o2.month) return o1.month.compare(o2.month)
    else if (o1.day != o2.day) return o1.day.compare(o2.day)

    o1.timestamp.compare(o2.timestamp)
  }
}