package controller

import java.security.InvalidParameterException
import java.util
import java.util.Comparator

import scala.util.control.Breaks._
import controller.comparator.DateComparator
import controller.comparator.group.{YearComparator, YearMonthComparator, YearMonthDayComparator}
import model.{CandlePrice, CandlePriceChange}

import scala.collection.mutable.ListBuffer

/**
  * Controller class
  **/
class CandleController(){
  /**
    * Computes price change for candles
    *
    * @param candles - list from candles to be processed
    * @return list from price changes
    **/
  def getPriceChange(candles: Array[CandlePrice]): Array[CandlePriceChange] = {
    val candlePriceChanges: ListBuffer[CandlePriceChange] = ListBuffer[CandlePriceChange]()

    for (candle <- candles) {
      candlePriceChanges += new CandlePriceChange(candle)
    }
    candlePriceChanges.toArray
  }

  /**
    * Groups candles by year
    **/
  def groupByYear(candles: Array[CandlePrice]): Array[CandlePrice] = groupBy(candles, new YearComparator)

  /**
    * Groups candles by year and month
    **/
  def groupByYearMonth(candles: Array[CandlePrice]): Array[CandlePrice] = groupBy(candles, new YearMonthComparator)

  /**
    * Groups candles by year, month and day
    **/
  def groupByYearMonthDay(candles: Array[CandlePrice]): Array[CandlePrice] = groupBy(candles, new YearMonthDayComparator)

  /**
    * Groups candles by input parameter.
    * Each group is represented as new CandlePrice object,
    * with time and openPrice as start time and price from the group
    * and closePrice as finish price from the group.
    **/
  private def groupBy(candles: Array[CandlePrice], comparator: Comparator[CandlePrice]): Array[CandlePrice] = {
    var groupedCandles: ListBuffer[CandlePrice] = ListBuffer[CandlePrice]()

    if (candles.size == 0) throw new InvalidParameterException("Empty candle list were given")

    candles.sorted(new DateComparator)

    var i: Int = 0

    while( i < candles.size ) {
      val startCandle: CandlePrice = candles(i)

      breakable {
        while(i < candles.size) {
          val currentCandle: CandlePrice = candles(i)

          if (comparator.compare(startCandle, currentCandle) != 0) break

          i += 1
        }
      }
      val finishCandle: CandlePrice = candles(i - 1)
      val groupedCandle: CandlePrice = new CandlePrice(startCandle)
      groupedCandle.closePrice = finishCandle.closePrice
      groupedCandles += groupedCandle
    }
    groupedCandles.toArray
  }
}
