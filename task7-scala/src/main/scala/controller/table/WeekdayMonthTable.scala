package controller.table

import java.security.InvalidParameterException
import java.time.DayOfWeek
import java.util

import controller.CandleController
import model.{CandlePrice, CandlePriceChange}

import scala.collection.mutable.ArrayBuffer

class WeekdayMonthTable(val candles: Array[CandlePrice]) {
  /**
    * Candle controller
    ***/
  val candleController = new CandleController()

  /**
    * List from price changes for each day
    **/
  var candleDayPriceChange: Array[CandlePriceChange] = _

  /**
    * Computes price change for each day
    **/
  def computeDayPriceChange(): Unit = {
    if (candles == null) throw new InvalidParameterException("Candle list is null")
    val candleYearMonthDay = candleController.groupByYearMonthDay(candles)
    candleDayPriceChange = candleController.getPriceChange(candleYearMonthDay)
  }


  def normalizeLine(line: (Int, Array[(Short, Double)])): (Int, Array[String]) = {
    (line._1, line._2.sortWith(_._1 < _._1).map(_._2).map("%.4f".format(_)))
  }

  def weekdayNumToString(weekdayNum: Int): String = {
    weekdayNum match {
      case 1 => "MONDAY"
      case 2 => "TUESDAY"
      case 3 => "WEDNESDAY"
      case 4 => "THURSDAY"
      case 5 => "FRIDAY"
      case _ => "WEEKEND"
    }
  }

  def her = {
    candleDayPriceChange.groupBy(priceChange => (priceChange.month, priceChange.getDayOfWeek))
      .mapValues(_.map(_.priceChange))
      .mapValues(arr => arr.sum / arr.length)
      .toArray
      .map(key_value => (key_value._1._2, (key_value._1._1, key_value._2)))
  }


  /**
    * Creates and returns required by task 2 table
    **/
  def getTable: Array[Array[String]] = {
    if (candleDayPriceChange == null) throw new InvalidParameterException("Candle month-year price change is null")

    val groupedData =candleDayPriceChange.groupBy(priceChange => (priceChange.month, priceChange.getDayOfWeek))
      .mapValues(_.map(_.priceChange))
      .mapValues(arr => arr.sum / arr.length)
      .toArray
      .map(key_value => (key_value._1._2, (key_value._1._1, key_value._2)))
      .groupBy(_._1)

    val weekdayAverage = groupedData.mapValues(_.map(key_value => key_value._2._2))
      .mapValues(arr => arr.sum / arr.length)

    val tableCore = groupedData.mapValues(_.map(key_value => key_value._2))
      .map(normalizeLine)
      .toArray
      .sortWith(_._1 < _._1)

    var bufTableCore = new ArrayBuffer[Array[String]]()

    for (coreLine <- tableCore) {
      bufTableCore += weekdayNumToString(coreLine._1) +: coreLine._2 :+ "%.4f".format(weekdayAverage(coreLine._1))
    }

    bufTableCore.toArray
  }

}
