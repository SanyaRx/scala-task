package controller.table

import controller.CandleController
import model.CandlePrice
import model.CandlePriceChange
import java.security.InvalidParameterException

import scala.collection.mutable.ArrayBuffer



/**
  * Class that creates table for the first task
  **/
class YearMonthTable(val candles: Array[CandlePrice]) {
  /**
    * Candle controller
    ***/
  val candleController = new CandleController()

  /**
    * List from candles grouped by year and month
    **/
  var candleYearMonth: Array[CandlePrice] = _
  /**
    * List from price changes for each year-month group
    **/
  var candleYearMonthPriceChange: Array[CandlePriceChange] = _
  /**
    * List from price changes for each year group
    **/
  var candleYearPriceChange: Array[CandlePriceChange] = _
  /**
    * Average price change for each month
    **/
  var monthAverage: Array[Double] = _

  /**
    * Computes price change for each year-month group
    **/
  def computePriceChangeYearMonth(): Unit = {
    if (candles == null) throw new InvalidParameterException("Candle list is null")
    candleYearMonth = candleController.groupByYearMonth(candles)
    candleYearMonthPriceChange = candleController.getPriceChange(candleYearMonth)
  }

  /**
    * Computes price change for each year group
    **/
  def computePriceChangeYear(): Unit = {
    if (candles == null && candleYearMonth == null) throw new InvalidParameterException("Candle list is null")
    if (candleYearMonth != null) candleYearPriceChange = candleController.getPriceChange(candleController.groupByYear(candleYearMonth))
    else candleYearPriceChange = candleController.getPriceChange(candleController.groupByYear(candles))
  }

  /**
    * Computes average price change for each month
    **/
  def computeMonthAveragePriceChange(): Array[Double] = {
    candleYearMonthPriceChange.groupBy(_.month)
      .mapValues(_.map(_.priceChange))
      .mapValues(arr => arr.sum / arr.length)
      .toArray
      .sortWith(_._1 < _._1)
      .map(_._2)
  }

  /**
    * Runs pipeline to compute all required data
    **/
  def computeAll(): Unit = {
    computePriceChangeYearMonth()
    computePriceChangeYear()
    computeMonthAveragePriceChange()
  }

  def tupleToArray(tuple:(Short, Array[String])): Array[String] = tuple._1.toString +: tuple._2


  /**
    * Creates and returns table required by the first task
    **/
  def getTable(short: Boolean): Array[Array[String]] = {
    if (candleYearMonthPriceChange == null || candleYearPriceChange == null)
      throw new InvalidParameterException("Candle month-year price change is null")

    val tableCore = candleYearMonthPriceChange.groupBy(_.year)
      .mapValues(_.sortWith(_.month < _.month).map(pc => "%.4f".format(pc.priceChange)))

    val monthAverage = computeMonthAveragePriceChange().map("%.4f".format(_))

    var bufTableCore = new ArrayBuffer[Array[String]]()

    for (priceChange <- candleYearPriceChange) {
      bufTableCore +=
        ( priceChange.year.toString // year
          +: tableCore(priceChange.year) // month price change
          :+ "%.4f".format(priceChange.priceChange)) // year price change
    }

    if (!short) bufTableCore += ("TOTAL" +: monthAverage :+ "0")

    bufTableCore.toArray
  }




}
