package controller

import java.io.{File, FileInputStream, InputStream}
import java.security.{DigestInputStream, MessageDigest}

import com.typesafe.scalalogging.Logger
import controller.config.{ConfigReader, DBConfigReader, DataDescriptionReader}
import controller.database.{DBController, MetaInfoController}
import controller.table.{WeekdayMonthTable, YearMonthTable}
import model.CandlePrice

class ScalaController(configPath: String, auditConfigPath: String) {

  val logger = Logger("controller")

  def computeHash(file: File): String = {
    val buffer = new Array[Byte](8192)
    val md5 = MessageDigest.getInstance("MD5")

    val dis = new DigestInputStream(new FileInputStream(file), md5)
    try { while (dis.read(buffer) != -1) { } } finally { dis.close() }

    md5.digest.map("%02x".format(_)).mkString
  }



  def readCSV(file: File, header: Boolean, parser: DataParserCSV) : Array[CandlePrice] = {
    val bufferedSource = io.Source.fromFile(file)
    val linesToDrop = if (header) 1 else 0

    val result = bufferedSource.getLines
      .drop(linesToDrop)
      .map(parser.parse)
      .toArray

    bufferedSource.close

    result
  }


  def inputToFile(is: InputStream, f: File) {
    val in = scala.io.Source.fromInputStream(is)
    val out = new java.io.PrintWriter(f)
    try { in.getLines().foreach(out.println(_)) }
    finally { out.close }
  }



  def executeTask(): Unit = {

    val configReader = new ConfigReader(configPath)
    configReader.readConfig()

    val sqlConfigReader = new DBConfigReader(auditConfigPath)
    sqlConfigReader.parse()

    val sourceBucketController = new S3BucketController(configReader.sourceUser)
    val objectsSummaries = sourceBucketController.getListOfObjectSummaries(configReader.sourcePath, configReader.sourceBucketName)

    val dataConfigFilePath = configReader.sourcePath + "/" + configReader.dataConfigFile
    val dataConfigFile = sourceBucketController.readS3Object(configReader.sourceBucketName,
      dataConfigFilePath )

    val dataConfigReader = new DataDescriptionReader(dataConfigFile)
    dataConfigReader.parse()

    val objectKeys = objectsSummaries.stream()
      .map[String](_.getKey)
      .filter(_.endsWith(".csv"))
      .toArray
      .map(_.toString)

    val dbController = new DBController(configReader.connector)
    val metaInfoDBController = new MetaInfoController(sqlConfigReader.connector)


    for (key <- objectKeys) {

      val source = "s3a//:" + configReader.sourceBucketName + "/" + key
      val destination = configReader.databaseURL

      logger.info(s"Working with file: $source")

      val sourceInputStream = sourceBucketController.readS3Object(configReader.sourceBucketName, key)

      val tempDataFile = new File(key.split("/").last)

      logger.info("Downloading file to local directory")

      inputToFile(sourceInputStream, tempDataFile)

      val sourceHash = metaInfoDBController.getHashSum(source, destination)
      val currentSourceHash = computeHash(tempDataFile)

      if (sourceHash == null || !sourceHash.equals(currentSourceHash)) {

        if (sourceHash != null) {
          dbController.deleteOutdatedRows(source)
          logger.info("Drop report row associated with file: " + source )
        }

        val startDate = System.currentTimeMillis()

        val currentID = metaInfoDBController.insertInfo(source, configReader.databaseURL, startDate, "task1")

        val candles = readCSV(tempDataFile, header = true, dataConfigReader.dataParser)

        val yearMonthTable = new YearMonthTable(candles)
        yearMonthTable.computeAll()

        val firstTable = yearMonthTable.getTable(short = true)


        for (line <- firstTable) {
          logger.info("Inserting: " + (line :+ dataConfigReader.currency :+ source).mkString(" "))
          dbController.insertYearMonth(line :+ dataConfigReader.currency :+ source)
        }

        val finishDate = System.currentTimeMillis()


        val processedRows = candles.length

        metaInfoDBController.updateInfo(finishDate, candles.length, processedRows, currentSourceHash, currentID)

      } else {
        logger.info("Report is already up to date")
      }
      tempDataFile.delete()

    }
  }

}
